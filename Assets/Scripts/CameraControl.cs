﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    GameObject cam;
    GameObject camNeck;
    GameObject camLookAt;
    float speed = 50f;

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.Find("MainCamera");
        camNeck = GameObject.Find("MainCameraNeck");
        camLookAt = GameObject.Find("CameraCenter");
    }

    // Update is called once per frame
    void Update()
    {
        CheckInputs();
    }

    void CheckInputs()
    {
        if(Input.GetKey(KeyCode.W))
        {
            if(cam.transform.position.y <= 10)
            {
                cam.transform.position += new Vector3(0, speed * Time.deltaTime/5, 0);
            }
        }
        else if (Input.GetKey(KeyCode.S))
        {
            if (cam.transform.position.y >= 2)
            {
                cam.transform.position -= new Vector3(0, speed * Time.deltaTime/5, 0);
            }
        }
        if (Input.GetKey(KeyCode.A)) 
        {
            if(camNeck.transform.rotation.y <0.65f)
                camNeck.transform.Rotate(0, speed * Time.deltaTime, 0);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            if (camNeck.transform.rotation.y > -0.65f)
                camNeck.transform.Rotate(0, -speed * Time.deltaTime, 0);
        }
        cam.transform.LookAt(camLookAt.transform);
    }
}
