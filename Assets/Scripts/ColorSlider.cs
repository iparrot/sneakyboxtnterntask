﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum ColorEnum { R, G, B, H, S, V, A };

public class ColorSlider : MonoBehaviour, IPointerUpHandler
{
    //[SerializeField]
    public ColorEnum colorEnum;
    char colorChar;

    InputField textInputField;
    Text textPlaceholder;
    Text textInput;
    Text textColorChar;
    public Slider slider { get; set; }
    Image backGround;
    Image handle;
    ColorPicker colorPicker;

    void GetAllComponents()
    {
        textPlaceholder = transform.Find("InputField").Find("Placeholder").GetComponent<Text>();
        textInput = transform.Find("InputField").Find("TextInput").GetComponent<Text>();
        textColorChar = transform.Find("TextChar").GetComponent<Text>();
        slider = transform.GetComponent<Slider>();
        colorPicker = GameObject.Find("ColorPicker_Panel").GetComponent<ColorPicker>();
        textInputField = transform.Find("InputField").GetComponent<InputField>();
    }

    void GetAllListeners()
    {
        textInputField.onEndEdit.AddListener(delegate { TextValueChanged(); });
    }

    private void Awake()
    {
        GetAllComponents();
    }

    // Start is called before the first frame update
    void Start()
    {
        GetAllListeners();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("The slider click was released: " + eventData.ToString());
        SliderValueChange();
    }

    void SliderValueChange()
    {
        textPlaceholder.text = "" + GetSliderValueX255();
        textInputField.text = "";
        if (colorEnum == ColorEnum.R || colorEnum == ColorEnum.G || colorEnum == ColorEnum.B || colorEnum == ColorEnum.A)
        {
            colorPicker.RGBAedit();
        }
        else if (colorEnum == ColorEnum.H || colorEnum == ColorEnum.S || colorEnum == ColorEnum.V)
        {
            colorPicker.HSVedit();
        }
    }

    void TextValueChanged()
    {
        float value = 0;
        float.TryParse(textInput.text, out value);
        slider.value = value/255;
        textInputField.text = "";
        textPlaceholder.text = "" + value;
        
        if (colorEnum == ColorEnum.R || colorEnum == ColorEnum.G || colorEnum == ColorEnum.B || colorEnum == ColorEnum.A)
        {
            colorPicker.RGBAedit();
        }
        else if (colorEnum == ColorEnum.H || colorEnum == ColorEnum.S || colorEnum == ColorEnum.V)
        {
            colorPicker.HSVedit();
        }
    }

    Color GetEnumColor()
    {
        Color color = new Color(0, 0, 0, 0);
        if (colorEnum == ColorEnum.R) color = new Color(255, 0, 0);
        else if (colorEnum == ColorEnum.G) color = new Color(0, 255, 0);
        else if (colorEnum == ColorEnum.B) color = new Color(0, 0, 255);
        return color;
    }

    public float GetSliderValue()
    {
        return slider.value;
    }

    public int GetSliderValueX255()
    {
        return Mathf.RoundToInt(slider.value * 255);
    }

    public void UpdateColorValues(Color c, float h, float s, float v)
    {
        if(colorEnum == ColorEnum.R)
        {
            slider.value = c.r;
            textPlaceholder.text = "" + GetSliderValueX255();
        }
        else if (colorEnum == ColorEnum.G)
        {
            slider.value = c.g;
            textPlaceholder.text = "" + GetSliderValueX255();
        }
        else if (colorEnum == ColorEnum.B)
        {
            slider.value = c.b;
            textPlaceholder.text = "" + GetSliderValueX255();
        }
        else if (colorEnum == ColorEnum.H)
        {
            slider.value = h;
            textPlaceholder.text = "" + (h*255f);
        }
        else if (colorEnum == ColorEnum.S)
        {
            slider.value = s;
            textPlaceholder.text = "" + (s*255f);
        }
        else if (colorEnum == ColorEnum.V)
        {
            slider.value = v;
            textPlaceholder.text = "" + (v*255f);
        }
        else if (colorEnum == ColorEnum.A)
        {
            slider.value = c.a;
            textPlaceholder.text = "" + GetSliderValueX255();
        }
    }
}
