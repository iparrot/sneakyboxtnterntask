﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    enum ButtonState { Empty, Save, Load, New, Cube, Sphere, Cylinder, Scale, Color, Move0, Move1, Delete}; 
    ButtonState state;
    string mapDataPath = "MapData.bin";
    bool isMapSaved = true;

    Button buttonSave, buttonLoad, buttonNew;
    Button buttonCube, buttonSphere, buttonCylinder;
    Button buttonsScale, buttonColor, buttonMove, buttonDelete;
    Toggle toggleGrid, toggleStayOnState;
    Text textInfo;

    public GameObject cube, sphere, cylinder;
    private GameObject cameraCube, cameraSphere, cameraCylinder, movingObject;

    Grid grid;

    GameObject ColorPickerPanel;
    GameObject ScalePanel;
    GameObject warning_panel;
    Text text_warningName;
    Button buttonWarningExit, buttonWarningConfirm;

    Text text_ScaleX, text_ScaleY, text_ScaleZ;
    //Text text_ScaleXPlaceHolder, text_ScaleYPlaceHolder, text_ScaleZPlaceHolder;
    ColorPicker colorPicker;

    void GetAllComponents()
    {
        buttonSave = GameObject.Find("ButtonSave").GetComponent<Button>();
        buttonLoad = GameObject.Find("ButtonLoad").GetComponent<Button>();
        buttonNew = GameObject.Find("ButtonNew").GetComponent<Button>();
        buttonCube = GameObject.Find("ButtonCube").GetComponent<Button>();
        buttonSphere = GameObject.Find("ButtonSphere").GetComponent<Button>();
        buttonCylinder = GameObject.Find("ButtonCylinder").GetComponent<Button>();
        buttonsScale = GameObject.Find("ButtonScale").GetComponent<Button>();
        buttonColor = GameObject.Find("ButtonColor").GetComponent<Button>();
        buttonMove = GameObject.Find("ButtonMove").GetComponent<Button>();
        buttonDelete = GameObject.Find("ButtonDelete").GetComponent<Button>();
        toggleGrid = GameObject.Find("ToggleGrid").GetComponent<Toggle>();
        toggleStayOnState = GameObject.Find("ToggleStay").GetComponent<Toggle>();
        textInfo = GameObject.Find("TextInfo").GetComponent<Text>();
        text_warningName = GameObject.Find("WarningName_Text").GetComponent<Text>();

        cameraCube = GameObject.Find("CameraCube");
        cameraSphere = GameObject.Find("CameraSphere");
        cameraCylinder = GameObject.Find("CameraCylinder");

        warning_panel = GameObject.Find("Warning_Panel");
        text_warningName = GameObject.Find("WarningName_Text").GetComponent<Text>();
        buttonWarningExit = GameObject.Find("Warning_ButtonExit").GetComponent<Button>();
        buttonWarningConfirm = GameObject.Find("Warning_ButtonConfirm").GetComponent<Button>();

        ScalePanel = GameObject.Find("Scale_Panel");
        text_ScaleX = GameObject.Find("Text_ScaleX").GetComponent<Text>();
        text_ScaleY = GameObject.Find("Text_ScaleY").GetComponent<Text>();
        text_ScaleZ = GameObject.Find("Text_ScaleZ").GetComponent<Text>();

        ColorPickerPanel = GameObject.Find("ColorPicker_Panel");
        colorPicker = GameObject.Find("ColorPicker_Panel").GetComponent<ColorPicker>();
    }

    void AddAllListeners()
    {
        buttonSave.onClick.AddListener(ButtonSaveClick);
        buttonLoad.onClick.AddListener(ButtonLoadClick);
        buttonNew.onClick.AddListener(ButtonNewClick);
        buttonCube.onClick.AddListener(ButtonCubeClick);
        buttonSphere.onClick.AddListener(ButtonSphereClick);
        buttonCylinder.onClick.AddListener(ButtonCylinderClick);
        buttonsScale.onClick.AddListener(ButtonScaleClick);
        buttonColor.onClick.AddListener(ButtonColorClick);
        buttonMove.onClick.AddListener(ButtonMoveClick);
        buttonDelete.onClick.AddListener(ButtonDeleteClick);

        buttonWarningExit.onClick.AddListener(ButtonWarningExitClick);
        buttonWarningConfirm.onClick.AddListener(ButtonWarningConfirmClick);
    }

    private void Awake()
    {
        GetAllComponents();
        AddAllListeners();
        grid = FindObjectOfType<Grid>();
    }

    private void Start()
    {
        warning_panel.SetActive(false);
        ColorPickerPanel.SetActive(false);
        ScalePanel.SetActive(false);
        
    }

    void Update()
    {
        CheckMouseButtonClick();
        CheckMousePosition();
        CheckCamObjects();
        if (Input.GetKey("escape"))
        {
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #else
            Application.Quit();
            #endif
        }
    }


    void PlaceFigure(Vector3 point)
    {
        Vector3 finalPosition;
        if (toggleGrid.isOn) { finalPosition = grid.GetNearestPointOnGrid(point); }
        else { finalPosition = point; }
        if (state == ButtonState.Cube) Instantiate(cube, finalPosition, Quaternion.identity).GetComponent<Renderer>().material.color = GetPaletteColor();
        else if (state == ButtonState.Sphere) Instantiate(sphere, finalPosition, Quaternion.identity).GetComponent<Renderer>().material.color = GetPaletteColor();
        else if (state == ButtonState.Cylinder) Instantiate(cylinder, finalPosition, Quaternion.identity).GetComponent<Renderer>().material.color = GetPaletteColor();
    }

    void LoadFigure(Figure figure)
    {
        GameObject gObject; 
        if (figure.GetLayerId() == 10)
        {
            gObject = Instantiate(cube, figure.GetVector3(), figure.GetQuaternion());
        }
        else if (figure.GetLayerId() == 11)
        {
            gObject = Instantiate(sphere, figure.GetVector3(), figure.GetQuaternion());
        }
        else //if (figure.GetLayerId() == 12)
        {
            gObject = Instantiate(cylinder, figure.GetVector3(), figure.GetQuaternion());
        }
        gObject.GetComponent<Renderer>().material.color = figure.GetColor();
        gObject.transform.localScale = figure.GetScale();
    }

    void MoveFigure(Vector3 point)
    {
        Vector3 finalPosition;
        if (toggleGrid.isOn)
        {
            finalPosition = grid.GetNearestPointOnGrid(point);
        }
        else
        {
            finalPosition = point;
        }
        movingObject.transform.position = finalPosition;
        movingObject.GetComponent<Collider>().enabled = true;
    }

    void CheckMouseButtonClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo))
            {
                if (state == ButtonState.Cube || state == ButtonState.Sphere || state == ButtonState.Cylinder)
                {
                    PlaceFigure(hitInfo.point + hitInfo.normal / 2);
                    ClearButtonState();
                }
                if(state == ButtonState.Scale && IsNotMapLayer(hitInfo.transform.gameObject))
                {
                    hitInfo.transform.localScale = GetScale();
                }
                if (state == ButtonState.Color && IsNotMapLayer(hitInfo.transform.gameObject))
                {
                    hitInfo.transform.gameObject.GetComponent<Renderer>().material.color = GetPaletteColor();
                    //hitInfo.transform.gameObject.GetComponent<Renderer>().material.color = 
                    ClearButtonState();
                }
                if (state == ButtonState.Move0 && IsNotMapLayer(hitInfo.transform.gameObject))
                {
                    movingObject = hitInfo.transform.gameObject;
                    movingObject.GetComponent<Collider>().enabled = false;
                    textInfo.text = "Place object";
                    state = ButtonState.Move1;
                }
                else if(state == ButtonState.Move1)
                {
                    MoveFigure(hitInfo.point + hitInfo.normal / 2);
                    ClearButtonState();
                }
                if (state == ButtonState.Delete && IsNotMapLayer(hitInfo.transform.gameObject))
                {
                    Destroy(hitInfo.transform.gameObject);
                    ClearButtonState();
                }
                isMapSaved = false;
            }
        }
    }

    void CheckMousePosition()
    {
        RaycastHit hitInfo;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if (Physics.Raycast(ray, out hitInfo))
        {
            Vector3 finalPosition;
            if (toggleGrid.isOn)
            {
                finalPosition = grid.GetNearestPointOnGrid(hitInfo.point + hitInfo.normal / 2);
            }
            else
            {
                finalPosition = hitInfo.point + hitInfo.normal / 2;
            }

            if (state == ButtonState.Cube)
            {
                cameraCube.transform.position = finalPosition;
                if (cameraCube.GetComponent<Renderer>().material.color != GetPaletteColor()) cameraCube.GetComponent<Renderer>().material.color = GetPaletteColor();
            }
            else if (state == ButtonState.Sphere)
            {
                cameraSphere.transform.position = finalPosition;
                if (cameraSphere.GetComponent<Renderer>().material.color != GetPaletteColor()) cameraSphere.GetComponent<Renderer>().material.color = GetPaletteColor();
            }
            else if (state == ButtonState.Cylinder)
            {
                cameraCylinder.transform.position = finalPosition;
                if (cameraCylinder.GetComponent<Renderer>().material.color != GetPaletteColor()) cameraCylinder.GetComponent<Renderer>().material.color = GetPaletteColor();
            }
            else if (cameraCube.active || cameraSphere.active || cameraCylinder.active)
            {
                cameraCube.SetActive(false);
                cameraSphere.SetActive(false);
                cameraCylinder.SetActive(false);
            }

            if(state == ButtonState.Move1)
            {
                movingObject.transform.position = finalPosition;
            }
        }
    }

    void CheckCamObjects()
    {
        if (cameraCube.active && state != ButtonState.Cube)
        {
            cameraCube.SetActive(false);
        }
        if (cameraSphere.active && state != ButtonState.Sphere)
        {
            cameraSphere.SetActive(false);
        }
        if (cameraCylinder.active && state != ButtonState.Cylinder)
        {
            cameraCylinder.SetActive(false);
        }
    }

    bool IsNotMapLayer(GameObject gameObject)
    {
        if (gameObject.layer != 9) return true;
        else return false;
    }

    Color GetPaletteColor()
    {
        return colorPicker.mainColor; 
    }

    Vector3 GetScale()
    {
        float x = 1, y = 1, z = 1;
        float.TryParse(text_ScaleX.text, out x);
        float.TryParse(text_ScaleY.text, out y);
        float.TryParse(text_ScaleZ.text, out z);
        if (x == 0) x = 1;
        if (y == 0) y = 1;
        if (z == 0) z = 1;
        return new Vector3(x,y,z);
    }


    void NewWarning(string warningName)
    {
        text_warningName.text = warningName;
        //if(text_warningName.text.Length / 23 > 2)
        //{
        //    //warning_panel.GetComponent<RectTransform>().sizeDelta = new Vector2(200f, 70f + 20 * (text_warningName.text.Length / 23)); 
        //}
        warning_panel.SetActive(true);
    }

    /// <summary> 
    /// UI Buttons ///////////////////////////////////////////////////////////////////////
    /// </summary>

    void ButtonSaveClick()
    {
        if (!File.Exists(mapDataPath))
        {
            SaveMap();
        }
        else if (!isMapSaved)
        {
            state = ButtonState.Save;
            NewWarning("Are you sure you want to save?");
        }
    }
    
    void ButtonLoadClick()
    {
        if (isMapSaved) //GameObject.FindGameObjectsWithTag("Figure").Length == 0 
        {
            LoadMap();
        }
        else
        {
            state = ButtonState.Load;
            NewWarning("Load saved scene? Current scene will be lost.");
        }
    }

    void ButtonNewClick()
    {
        if(isMapSaved)
        {
            LoadNewMap();
        }
        else
        {
            state = ButtonState.New;
            NewWarning("Are you sure you want to discard changes?");
        }
    }

    void ButtonCubeClick()
    {
        if (state == ButtonState.Cube)
        {
            state = ButtonState.Empty;
            textInfo.text = "";
        }
        else
        {
            state = ButtonState.Cube;
            cameraCube.SetActive(true);
            textInfo.text = "Place cube";
        }
    }

    void ButtonSphereClick()
    {
        if (state == ButtonState.Sphere)
        {
            state = ButtonState.Empty;
            textInfo.text = "";
        }
        else
        {
            state = ButtonState.Sphere;
            cameraSphere.SetActive(true);
            textInfo.text = "Place sphere";
        }
    }

    void ButtonCylinderClick()
    {
        if (state == ButtonState.Cylinder)
        {
            state = ButtonState.Empty;
            textInfo.text = "";
        }
        else
        {
            state = ButtonState.Cylinder;
            cameraCylinder.SetActive(true);
            textInfo.text = "Place cylinder";
        }
    }

    void ButtonScaleClick()
    {
        if (state == ButtonState.Scale)
        {
            state = ButtonState.Empty;
            ScalePanel.SetActive(false);
            textInfo.text = "";
        }
        else
        {
            state = ButtonState.Scale;
            if (ColorPickerPanel.active) ColorPickerPanel.SetActive(false);
            ScalePanel.SetActive(true);
            textInfo.text = "Change scale";
        }
    }

    void ButtonColorClick()
    {
        if (state == ButtonState.Color)
        {
            state = ButtonState.Empty;
            ColorPickerPanel.SetActive(false);
            textInfo.text = "";
        }
        else
        {
            state = ButtonState.Color;
            if (ScalePanel.active) ScalePanel.SetActive(false);
            ColorPickerPanel.SetActive(true);
            textInfo.text = "Change color";
        }
    }

    void ButtonMoveClick()
    {
        if (state == ButtonState.Move0)
        {
            state = ButtonState.Empty;
            textInfo.text = "";
        }
        else
        {
            state = ButtonState.Move0;
            textInfo.text = "Move object";
        }
    }

    void ButtonDeleteClick()
    {
        if (state == ButtonState.Delete)
        {
            state = ButtonState.Empty;
            textInfo.text = "";
        }
        else
        {
            state = ButtonState.Delete;
            textInfo.text = "Delete object";
        }
    }

    void ButtonWarningExitClick()
    {
        warning_panel.SetActive(false);
        ClearButtonState();
    }

    void ButtonWarningConfirmClick()
    {
        if (state == ButtonState.Save)
        {
            SaveMap();            
        }
        if (state == ButtonState.Load)
        {
            LoadMap();
        }
        if (state == ButtonState.New)
        {
            LoadNewMap();
        }
        warning_panel.SetActive(false);
        state = ButtonState.Empty;
    }

    void ClearButtonState()
    {
        if (toggleStayOnState.isOn)
        {
            if (state == ButtonState.Move1)
            {
                state = ButtonState.Move0;
            }
        }
        else
        {
            textInfo.text = "";
            state = ButtonState.Empty;
            if (state != ButtonState.Color) ColorPickerPanel.SetActive(false);
            if (state != ButtonState.Scale) ScalePanel.SetActive(false);
        }
    }

    /// <summary>
    /// ////////////////////////////////////////////////////////////////////////////////////
    /// </summary>

    void SaveMap()
    {
        GameObject[] obj = GameObject.FindGameObjectsWithTag("Figure");
        Figure[] figure = new Figure[obj.Length];
        for (int i = 0; i < figure.Length; i++)
        {
            figure[i] = new Figure(obj[i]);
        }
        IFormatter formatter = new BinaryFormatter();
        Stream stream = new FileStream(mapDataPath, FileMode.Create, FileAccess.Write, FileShare.None);
        formatter.Serialize(stream, figure);
        stream.Close();
        isMapSaved = true;
        textInfo.text = "Room saved";
    }

    void LoadMap()
    {
        if (File.Exists(mapDataPath))
        {
            textInfo.text = "Room loading";
            GameObject[] obj = GameObject.FindGameObjectsWithTag("Figure");
            for (int i = 0; i < obj.Length; i++)
            {
                Destroy(obj[i]);
            }
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(mapDataPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            Figure[] oo = (Figure[])formatter.Deserialize(stream);
            for (int i = 0; i < oo.Length; i++)
            {
                LoadFigure(oo[i]);
            }
            stream.Close();
            isMapSaved = true;
            textInfo.text = "Room loaded";
        }
        else textInfo.text = "There is nothing to load";
    }

    void LoadNewMap()
    {
        SceneManager.LoadScene(0);
        textInfo.text = "New room";
        isMapSaved = true;
    }
}
