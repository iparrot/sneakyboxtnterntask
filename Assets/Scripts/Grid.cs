﻿using UnityEngine;
using UnityEngine.UI;

public class Grid : MonoBehaviour
{
    Text text_Grid, placeHolder_Grid;

    [SerializeField]
    private float size = 1f;
    public float Size { get { return size; } }

    public Vector3 GetNearestPointOnGrid(Vector3 position)
    {
        position -= transform.position;
        int xCount = Mathf.RoundToInt(position.x / size);
        int yCount = Mathf.RoundToInt(position.y / size);
        int zCount = Mathf.RoundToInt(position.z / size);

        Vector3 result = new Vector3((float)xCount * size, (float)yCount * size, (float)zCount * size);
        result += transform.position;

        return result;
    }

    private void Awake()
    {
        text_Grid = GameObject.Find("Text_Grid").GetComponent<Text>();
        placeHolder_Grid = GameObject.Find("Placeholder_Grid").GetComponent<Text>();
        placeHolder_Grid.text = "" + size;
        text_Grid.text = "" + size;
    }
    // Start is called before the first frame update
    void Start()
    {
        size = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        float temp;
        float.TryParse(text_Grid.text, out temp); // if(OnValueChanged)
        if (temp != 0) size = temp;
    }
}
