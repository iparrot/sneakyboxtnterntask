﻿using UnityEngine;
[System.Serializable]

public class Figure
{
    int layerId;
    float positionX, positionY, positionZ;
    float rotationX, rotationY, rotationZ, rotationW;
    float colorRed, colorGreen, colorBlue, colorAlpha;
    float scaleX, scaleY, scaleZ;

    public Figure(GameObject gObject)
    {
        positionX = gObject.transform.position.x;
        positionY = gObject.transform.position.y;
        positionZ = gObject.transform.position.z;
        rotationX = gObject.transform.rotation.x;
        rotationY = gObject.transform.rotation.y;
        rotationZ = gObject.transform.rotation.z;
        rotationW = gObject.transform.rotation.w;
        colorRed = gObject.GetComponent<Renderer>().material.color.r;
        colorGreen = gObject.GetComponent<Renderer>().material.color.g;
        colorBlue = gObject.GetComponent<Renderer>().material.color.b;
        colorAlpha = gObject.GetComponent<Renderer>().material.color.a;
        scaleX = gObject.transform.localScale.x;
        scaleY = gObject.transform.localScale.y;
        scaleZ = gObject.transform.localScale.z;
        layerId = gObject.layer;
    }

    public Vector3 GetVector3()
    {
        return new Vector3(positionX, positionY, positionZ);
    }

    public Quaternion GetQuaternion()
    {
        return new Quaternion(rotationX, rotationY, rotationZ, rotationW);
    }

    public Color GetColor()
    {
        return new Color(colorRed, colorGreen, colorBlue, colorAlpha);
    }

    public Vector3 GetScale()
    {
        return new Vector3(scaleX, scaleY, scaleZ);
    }

    public int GetLayerId() { return layerId; }
}
