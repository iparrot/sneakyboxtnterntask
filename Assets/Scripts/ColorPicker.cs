﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPicker : MonoBehaviour
{
    [SerializeField]
    ColorSlider[] sliders;
    Image colorImage;
    Text HexPlaceholder, HexTextInput;
    InputField hexInputField;
    
    public Color mainColor;

    public float h, s, v;

    void GetAllComponents()
    {
        HexPlaceholder = GameObject.Find("HexPlaceholder").GetComponent<Text>();
        HexTextInput = GameObject.Find("HexTextInput").GetComponent<Text>();
        colorImage = GameObject.Find("ColorImage").GetComponent<Image>();
        hexInputField = GameObject.Find("HexInputField").GetComponent<InputField>();
    }

    void GetAllListeners()
    {
        hexInputField.onEndEdit.AddListener(delegate { SetHexColor(); });
    }

    private void Awake()
    {
        GetAllComponents();
    }

    // Start is called before the first frame update
    void Start()
    {
        GetAllListeners();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RGBAedit()
    {
        Debug.Log("RGBAedit");
        mainColor = GetRGBAColor();
        HexPlaceholder.text = GetHexColor();
        hexInputField.text = "";
        HexTextInput.text = "";
        colorImage.color = mainColor;
        Color.RGBToHSV(mainColor, out h, out s, out v);
        UpdateHSVsliders();
    }

    public void HSVedit()
    {
        Debug.Log("HSVedit");
        GetHSVColor();
        mainColor = Color.HSVToRGB(h, s, v);
        HexPlaceholder.text = GetHexColor();
        hexInputField.text = ""; //?
        HexTextInput.text = "";
        colorImage.color = mainColor;
        UpdateRGBAsliders();
    }


    void SetHexColor()
    {
        UpdateMainColor(GetColorFromHex());
        Color.RGBToHSV(mainColor, out h, out s, out v);
        Debug.Log("" + mainColor + " "+ h + " " + s + " " + v + " " + GetHexColor());
        HexPlaceholder.text = GetHexColor();
        HexTextInput.text = "";
        hexInputField.text = "";        
        UpdateRGBAHSVsliders();
    }

    public Color GetRGBAColor()
    {
        return new Color(sliders[0].GetSliderValue(), sliders[1].GetSliderValue(), sliders[2].GetSliderValue(), sliders[6].GetSliderValue());
    }

    public void GetHSVColor()
    {
        h = sliders[3].GetSliderValue();
        s = sliders[4].GetSliderValue();
        v = sliders[5].GetSliderValue();
    }

    string GetHexColor()
    {
        return ColorUtility.ToHtmlStringRGBA(mainColor);
    }

    public void UpdateRGBAsliders()
    {
        for (int i = 0; i < 3; i++)
        {
            sliders[i].UpdateColorValues(mainColor, h, s, v);
        }
        sliders[6].UpdateColorValues(mainColor, h, s, v);
    }

    public void UpdateHSVsliders()
    {

        for (int i = 3; i < 6; i++)
        {
            sliders[i].UpdateColorValues(mainColor, h, s, v);
        }
    }

    void UpdateRGBAHSVsliders()
    {
        for (int i = 0; i < sliders.Length; i++)
        {
            sliders[i].UpdateColorValues(mainColor, h, s, v);
        }
    }

    void UpdateMainColor(Color color)
    {
        mainColor = color;
        colorImage.color = mainColor;
    }

    Color GetColorFromHex()
    {
        Color c;
        if(HexTextInput.text[0] != '#')
        {
            ColorUtility.TryParseHtmlString("#" + HexTextInput.text, out c);
        }
        else
        {
            ColorUtility.TryParseHtmlString(HexTextInput.text, out c);
        }
        return c;
    }
}
